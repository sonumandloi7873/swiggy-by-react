import React from 'react'
import { imageURL } from '../other/utilit'
import { useNavigate } from 'react-router-dom'
import { useDispatch,useSelector } from 'react-redux'
import { getRemove } from '../redux/Action'


const AddCart = () => {

  const navigate = useNavigate()
  const dispatch = useDispatch()
  const data = useSelector((state)=>state.reducer)
  console.log('data',data)

  return (
    <>
  <div className='flex flex-wrap p-10'>
    {
      data.map((item)=>{
        return(
          <>
            <div key={item?.id} className='w-[300px] h-[400px] shadow-md shadow-white rounded-xl'>
             <div className='w-[300px]'>
                <img src={`${imageURL}${item?.cloudinaryImageId}`}alt="" 
                className='w-[300px] h-[250px] rounded-lg'/>     
              </div>
              <div  className='p-3'>
                <h3 className='text-xl mb-2'>{item?.name}</h3>
                <div>
                <label className='mr-3'>{item?.avgRating}</label>
                <label>{item?.slaString}</label>
                </div>
                <p className='relative'>
                  <span>{item?.areaName}</span>
                 <button className="absolute right-1 border-2 px-3 py-1 bg-green-600 font-semibold rounded-lg"
                 onClick={()=>dispatch(getRemove(item?.id))}>remove</button></p>
              </div>
             </div>
          </>
        )
      })
    }
  </div>
      
    </>
  )
}

export default AddCart
