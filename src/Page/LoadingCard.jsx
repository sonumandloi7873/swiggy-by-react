import React from 'react'

const LoadingCard = () => {
  return (
    <>
        <div className='w-[300px] h-[380px] rounded-lg'>
             <div className='w-[300px] rounded-lg'>
                <img  alt="Food Image" 
                className='w-[300px] h-[250px] rounded-lg'/>     
              </div>
              <div  className='p-3'>
                <h2 className='mb-5 w-[100%] h-[20px] border-2 bg-gray-600 border-gray-600  rounded-md'>{}</h2>
                <p className='w-[100%] h-[20px] border-2 bg-gray-600 border-gray-600  rounded-md'>{}</p>
              </div>
             </div>
    </>
  )
}

export default LoadingCard
