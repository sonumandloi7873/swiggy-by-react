import { ADD_TO_CART, REMOVE_TO_CART, SET_USER_DATA } from "./Constant"


const initialState = {
    api:[],
    addtocart:[]
}


export const reducer = (state=initialState,action) =>{
    switch(action.type){
        case ADD_TO_CART:
            return[
                state.addtocart = action.data
                // state.addtocart =  state.addtocart.push(action.data)
            ]

        case REMOVE_TO_CART:
            let result = state.filter(item=>item.id != action.data)
            return[...result]

        case SET_USER_DATA:
            return[
                state.api = action.data
            ] 

            default :
            return state
    }

}