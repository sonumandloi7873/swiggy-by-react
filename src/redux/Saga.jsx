import { put, takeLatest } from 'redux-saga-effects'
import { REMOVE_TO_CART, SET_USER_DATA, USER_LIST } from './Constant'
import { apiData } from '../other/utilit'
import { useEffect } from 'react'


function* userList(){
    async function* callAPI(){ 
        let api = yield fetch(`${apiData}`)
        api = yield data.json()
        api = data?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle?.restaurants
        yield put({type:SET_USER_DATA,data:api})
        console.log("sonu",api)
       }
    useEffect(()=>{
        callAPI()
    },[])
}

function* saga(){
    yield takeLatest(USER_LIST,userList)
}

export default saga