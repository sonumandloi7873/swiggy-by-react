import { configureStore } from '@reduxjs/toolkit'
import RootReducer from './RootReducer'
import createSagaMiddlerware   from 'redux-saga'
import saga from './Saga'


const sagaMiddlerware = createSagaMiddlerware();
export const Store = configureStore({
        reducer:RootReducer,
        middleware:()=>[sagaMiddlerware]
})
sagaMiddlerware.run(saga)