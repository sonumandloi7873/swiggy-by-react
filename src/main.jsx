import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import {createBrowserRouter, RouterProvider  } from "react-router-dom"
import About from './Page/About.jsx'
import Home from './Swiggy/Home.jsx'
import OrderDeatils from './Swiggy/OrderDeatils.jsx'
import { Store } from './redux/Store.jsx'
import {Provider} from 'react-redux'
import AddCart from './Page/AddCart.jsx'

const router = createBrowserRouter([
  {
    path:"/",
    element:<App/>,
    children:[
      {
        path:"/",
        element:<Home/>

      },
      {
        path:"About",
        element:<About/>
      },
      {
        path:"OrderDeatils",
        element:<OrderDeatils/>
      },{
        path:"AddCart",
        element:<AddCart/>
      }

    ]
  }
])

const Main=()=>{
  return     <RouterProvider router={router}/>
  
}


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={Store}>
    <Main/>
</Provider>
  </React.StrictMode>,
)
