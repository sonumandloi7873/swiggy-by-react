import { Outlet, useLocation } from 'react-router-dom'
import Header from './Swiggy/Header'
import Footer from './Page/Footer'

function App() {  


  return (
    <>
     <Header/>
     <Outlet/>
     {/* <Footer/>   */}
    </>
  )
}

export default App
