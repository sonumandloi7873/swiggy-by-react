import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import {GiShoppingCart} from 'react-icons/gi'
import { useSelector} from 'react-redux'
import Home from './Home'

const Header = () => {
 const data = useSelector((state)=>state.reducer)

const navigate = useNavigate()
  const[btn,setBtn]=useState(false)
  // const [searchText, setSearchText]=useState("")
  // console.log(searchText)

  function clickBtn(){
    setBtn(!btn)
  }


  return (
    <div className='w-[100vw] flex justify-between p-3 px-5 bg-slate-600 text-white'>
        <div>
            <label> Logo</label>
        </div>
          <nav>
          <ul className='w-[100%] flex justify-end'>
          <li onClick={()=>navigate("/")} className='px-3'>Home</li>
            <li onClick={()=>navigate("/About",{state:{name:"sonu"}})} className='px-3'>about</li>
            <li className='px-3'>services</li>
            <li className='px-3'>help</li>
           <li> <button onClick={()=>clickBtn()} className='w-[110px]' >{btn ? "Sign-Out":"Sign-In"}</button></li>
           <li className='flex items-center relative '> <span className='absolute bottom-4 left-2 ' >{data.length}</span><GiShoppingCart className='text-2xl' onClick={()=>navigate('/AddCart')}/></li>
          </ul>
          </nav>

          <div className='hidden'>
       {/* <Home  searchText={searchText} /> */}
     </div>
    </div>
  )
}

export default Header
