import React, { useEffect, useState } from 'react'
import CardHome from './CardHome'
import { apiData } from '../other/utilit'
import LoadingCard from '../Page/LoadingCard'
import { useSelector } from 'react-redux'

const Home = () => {

const APIdata = useSelector((state)=>state.reducer)
console.log("APIdata",APIdata)

  const[data,setData]=useState([])
  const [searchFilter,setSearchFilter]=useState([])
  const[searchText,setSearchText]=useState("")

  const lodingData= [1,2,3,4,5,6,7]

    async function fetchJSON() {
      const response = await fetch(`${apiData}`);
      // const response = await fetch(`${dataApi}`);
      const apiJson = await response.json();
      const respo = apiJson?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle?.restaurants
      // const respo = apiJson?.menuItems
      setData(respo)
      setSearchFilter(respo)
    }
    
   useEffect(()=>{
    fetchJSON()
  },[]) 

useEffect(()=>{
  const filterSearch = data?.filter((item)=>item?.info?.name.toLowerCase().includes(searchText.toLowerCase()))
  // console.log("filterSearch",filterSearch)

  if(searchText?.length > 0 ){
    setSearchFilter(filterSearch)
  }else{
    setSearchFilter(data)
  }
},[searchText])


return(
  <>
   
  {data?.length === 0 ? (
    <div className='flex flex-wrap gap-10 p-3'>
      {
        lodingData.map((item)=>{
          return(<LoadingCard/>)
          })
      }
      </div>
  ) : (
    <>
    <input type="text" placeholder='search' onChange={(e)=>setSearchText(e.target.value)} 
    className='mx-5 my-3 h-[30px] p-2 rounded-md'  />

       <div className='flex flex-wrap gap-10 p-3'> 
          {
searchFilter.map((item)=>{
  return(
    <>
      <CardHome item={item}/>    
    </>
  )
  })  
          }
          </div>
        </>

        )
        }
  </>
  )
}

export default Home
