import React from 'react'
import { imageURL } from '../other/utilit'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { getAPI } from '../redux/Action'


const CardHome = (props) => {

  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { item = {} } = props || {}

  const { info = {} } = item || {}
  // console.log("item",info)

  const { cloudinaryImageId = "", name = "", avgRating = "", areaName = "", id = "", sla = {} } = info || {}
  const { slaString = "" } = sla || {}

  return (
    <>
      <div key={id} className='w-[300px] h-[400px] shadow-md shadow-white rounded-xl'>
        <div className='w-[300px]'>
          <img src={`${imageURL}${cloudinaryImageId}`} alt=""
            className='w-[300px] h-[250px] rounded-lg' />
        </div>
        <div className='p-3'>
          <h3 className='text-xl mb-2'>{name}</h3>
          <div>
            <label className='mr-3'>{avgRating}</label>
            <label>{slaString}</label>
          </div>
          <p className='relative'>
            <span>{areaName}</span>
            <button
              className="absolute
                         right-1
                         border-2
                         px-3 py-1
                       bg-green-600 
                         font-semibold
                         rounded-lg"
              onClick={() => dispatch(getAPI(info))}>ADD</button></p>
        </div>
      </div>

    </>
  )
}

export default CardHome
